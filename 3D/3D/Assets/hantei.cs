using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hantei : MonoBehaviour
{
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1.0f))
        {
            void OnTriggerEnter(Collider collider)
            {
                if (collider.gameObject.tag == "item")
                {
                    Destroy(gameObject);
                }
            }
            Debug.Log(hit.collider.gameObject.transform.position);
        }

    }
}