using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class oikake : MonoBehaviour
{
    NavMeshAgent Capsule_Nav;
    GameObject Destination;

    void Start()
    {
        //プレイヤーのNavMeshAgentを取得
        Capsule_Nav = GetComponent<NavMeshAgent>();
        //目的地のオブジェクトを取得
        Destination = GameObject.Find("unitychan_dynamic");
    }

    void Update()
    {
        //目的地を設定
        Capsule_Nav.SetDestination(Destination.transform.position);

    }

}
