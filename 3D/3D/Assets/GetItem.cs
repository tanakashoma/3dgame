using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetItem: MonoBehaviour
{

    public GameObject closetObject;
    public List<GameObject> objectsInSeachArea = new List<GameObject>();
    private Animator animator;
    public List<string> myItemList = new List<string>();

    private void Update()
    {
        CaluculateClosetObject();
    }

    void CaluculateClosetObject()
    {
        //一番近いアイテムを取得する
        float closetDistance = 1000000;
        for (int i = 0; i < objectsInSeachArea.Count; i++)
        {
            if (objectsInSeachArea[i] == null)
            {
                objectsInSeachArea.Remove(objectsInSeachArea[i]);
                return;
            }
            float distance = Vector3.Distance(transform.position, objectsInSeachArea[i].transform.position);
            if (closetDistance > distance)
            {
                closetDistance = distance;
                closetObject = objectsInSeachArea[i].gameObject;
            }
            //一定距離離れたらobjectsInSeachAreaからオブジェクトを取り除く。
            if (distance > 6f)
            {
                if (closetObject == objectsInSeachArea[i].gameObject)
                {
                    closetObject = null;
                }
                objectsInSeachArea.Remove(objectsInSeachArea[i]);
            }
        }

        //最も近いアイテムが一定の距離内にある場合、アイテムの説明UIを表示。Fキーを押すと拾える。
        if (closetObject == null) return;
        if (closetDistance < 1.5f)
        {
            //アイテム解説UI表示
            // uiManager.DisplayItemDescriptionUI();
            PickUp();
        }
        else
        {
            //アイテム解説UI非表示
            //uiManager.closeUnderText();
        }
    }

    void PickUp()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            animator.SetTrigger("pickup");
            myItemList.Add(closetObject.name);
            //objectsInSeachAreaからアイテムを取り除く。
            objectsInSeachArea.Remove(closetObject);
            Destroy(closetObject, 0.5f);
            closetObject = null;
            //アイテム解説UI非表示
            //uiManager.closeUnderText();
        }
    }
}
