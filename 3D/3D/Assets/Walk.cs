using UnityEngine;
using System.Collections;

public class Walk : MonoBehaviour
{

    private Animator animator;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("w"))
        {
            transform.position += transform.forward * 0.005f;
            animator.SetBool("is_walking", true);
        }
        else if (Input.GetKey("s"))
        {
            transform.position -= transform.forward * 0.005f;
            animator.SetBool("is_walking", true);
        }
        
        else if (Input.GetKey("d"))
        {
            transform.position += transform.right * 0.005f;
            animator.SetBool("is_walking", true);
        }
         else if (Input.GetKey("a"))
        {
            transform.position -= transform.right * 0.005f;
            animator.SetBool("is_walking", true);
        }
        else
        {
            animator.SetBool("is_walking", false);
        }
    }



}