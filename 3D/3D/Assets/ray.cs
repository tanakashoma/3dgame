using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ray : MonoBehaviour
{

    public bool itemray;

    // Use this for initialization
    void Start()
    {
        itemray = false;
    }

    // Update is called once per frame
    void Update()
    {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(mouseRay.origin, mouseRay.direction, Color.red, 0.8f);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit, 0.8f))
        {
            if (hit.collider.gameObject.tag == "item")
            {
                itemray = true;
            }
            else
            {
                itemray = false;
            }
        }
        else
        {
            itemray = false;
        }

    }
}